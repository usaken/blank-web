# BlankWeb Project

## 開発向け起動
1. パッケージインストール
1. コンテナを起動
1. ログ監視

```
# docker-compose run web /install.sh
# docker-compose run api npm install
# docker-compose up -d
# docker-compose logs -f
```

* コンテナを起動するとNode.jsのパッケージのインストールやサーバーの起動は自動で行われる
* サーバーが起動後、ブラウザで「localhost:3000」にアクセスするとトップページが表示される
* 「api/app/src」配下の「.ts」ファイルを更新すると自動でビルドしサーバーが再起動する


## 本番向け起動
1. イメージをビルド
1. コンテナを起動
1. ログ監視

```
# docker-compose -f docker-compose_production.yml build
# docker-compose -f docker-compose_production.yml up -d
# docker-compose -f docker-compose_production.yml logs -f
```

* 基本は開発向け起動と同じ
* ただし、ファイルを更新してもビルドや再起動は行わない
* サーバーが起動後、ブラウザで「localhost:3001」にアクセスするとトップページが表示される

## オプション
.envファイルで環境変数を指定することでいくつか

|環境変数名|内容|設定値|
|----|----|----|
|HOST_OS|DockerのホストOSを指定する。Windowsの場合は「windows」と指定する。指定するとOSによる違いを吸収してくれる（docker-composeのvolumesによるフォルダ共有をするとOS間で若干動作が異なるので意図した動きにならないことがある。その違いの吸収にこの設定値が利用される）<br>デフォルトMacを対象に動くようにしているのでMacは不要（おそらくLinuxもそのままで動くはず）|windows<br>（windows以外はコメントアウトのままで良い）|
