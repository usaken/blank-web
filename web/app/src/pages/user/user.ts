import axios from "axios";
import Vue from "vue";
import Component from "vue-class-component";
import * as Template from "./user.html";

@Template
@Component<User>({
  async mounted() {
    this.users = await this.getUser();
  },
})
export default class User extends Vue {
  public users = [];

  private async getUser() {
    return (await axios.get("/api/users")).data;
  }
}
