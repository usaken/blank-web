import Vue from "vue";
import Component from "vue-class-component";
import * as Template from "./top.html";

@Template
@Component<Top>({})
export default class Top extends Vue {
}
