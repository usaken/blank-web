import Vue from "vue";
import Component from "vue-class-component";
import * as Template from "./app.html";
import Navbar from "./components/navbar";
import router from "./router";

Vue.component("navbar", Navbar);

@Template
@Component<App>({
  router,
})
export default class App extends Vue {
}
