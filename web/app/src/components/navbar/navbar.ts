import Vue from "vue";
import Component from "vue-class-component";
import * as Template from "./navbar.html";

@Template
@Component<Navbar>({})
export default class Navbar extends Vue {
  public brand = "ブランク";
  public links = [
    { title: "トップ", name: "top" },
    { title: "ユーザー", name: "user" },
  ];
}
