import Vue from "vue";
import VueRouter from "vue-router";
import Top from "./pages/top";
import User from "./pages/user";

Vue.use(VueRouter);
const router = new VueRouter({
  mode: "history",
  routes: [{
    components: { default: Top },
    name: "top",
    path: "/",
  }, {
    components: { default: User },
    name: "user",
    path: "/user",
  }],
});

export default router;
