const gulp = require('gulp');
const del = require('del');
const path = require('path');
const conf = require('./gulp/conf');
const gutil = require('gulp-util');
const webpack = require('webpack-stream');
const browserSync = require('browser-sync');
const browserSyncSpa = require('browser-sync-spa');
const proxyMiddleware = require('http-proxy-middleware');
const extractTextPlugin = require('extract-text-webpack-plugin');

browserSync.use(browserSyncSpa());

function hostOsIsWindows() {
  const hostOs = process.env.HOST_OS;
  return !!hostOs && hostOs.toLowerCase() === 'windows';
}

function webpackWrapper(watch, callback) {
  var webpackOptions = {
    watch: watch,
    // HostOSがWindowsの場合は下記のオプションを有効にする
    // どうやらNFSの違いによりうまくwatchが機能しない模様
    // https://github.com/webpack/webpack-dev-server/issues/143
    watchOptions: {
      poll: hostOsIsWindows()
    },
    output: {
      filename: 'index.js'
    },
    resolve: {
      alias: {
        'vue$': 'vue/dist/vue.esm.js'
      },
      extensions: ['.ts', '.js']
    },
    module: {
      rules: [{
        test: /\.ts$/,
        exclude: /node_modules/,
        loaders: ['tslint-loader'],
        enforce: 'pre'
      }, {
        test: /\.ts$/,
        exclude: /node_modules/,
        loaders: ['ts-loader']
      }, {
        test: /\.woff2?$|\.ttf$|\.eot$|\.svg$|\.gif$/,
        loaders: ['file-loader']
      }, {
        test: /\.scss$/,
        loader: extractTextPlugin.extract(['css-loader', 'sass-loader'])
      }, {
        test: /\.html$/,
        loader: 'vue-template-loader',
        include: /src/
      }]
    },
    plugins: [
      new extractTextPlugin('index.css')
    ]
  };

  if (watch) {
    webpackOptions.devtool = 'inline-source-map';
  }

  var reload = false;
  var webpackChangeHandler = function(err, stats) {
    if (err) {
      conf.errorHandler('Webpack')(err);
    }
    gutil.log(stats.toString({
      colors: gutil.colors.supportsColor,
      chunks: false,
      hash: false,
      version: false
    }));
    if (reload) {
      browserSync.reload();
    }
    if (watch) {
      watch = false;
      reload = true;
      callback();
    }
  };

  var sources = [path.join(conf.paths.src, '/index.ts')];

  return gulp.src(sources)
    .pipe(webpack(webpackOptions, null, webpackChangeHandler))
    .pipe(gulp.dest(path.join(conf.paths.tmp, '/serve/app')));
}

gulp.task('ts', gulp.series(() => {
  return webpackWrapper(false);
}));

gulp.task('ts:watch', gulp.series((cb) => {
  return webpackWrapper(true, cb);
}));

gulp.task('clean', gulp.series(() => {
  return del([conf.paths.tmp, conf.paths.dist]);
}));

gulp.task('serve', gulp.series('clean', 'ts:watch', () => {
  browserSync.init({
    server: {
      baseDir: ['src', '.tmp/serve/app']
    },
    middleware: proxyMiddleware(['/api'], {
      target: 'http://api:3000',
      changeOrigin: true
    }),
    open: false
  });

  gulp.watch('src/index.html').on('change', browserSync.reload);
}));

gulp.task('build', gulp.series('ts', async () => {
  gulp.src([
    path.join(conf.paths.src, '/index.html'),
    path.join(conf.paths.tmp, '/serve/app/*'),
    path.join(conf.paths.src, '/favicon.ico')
  ]).pipe(gulp.dest(conf.paths.dist));
}));

gulp.task('default', gulp.series('clean', 'build'));
