#!/bin/bash

mkdir /apptmp
cd /apptmp
cp /app/package.json .
npm install
cp -rf /apptmp/node_modules /app
cp /apptmp/package-lock.json /app
