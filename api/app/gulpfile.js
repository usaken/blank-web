const gulp = require('gulp');
const del = require('del');
const ts = require('gulp-typescript');
const tsProject = ts.createProject(
  'tsconfig.json', {
    typescript: require('typescript')
  }
);
const nodemon = require('gulp-nodemon');
const path = require('path');

function hostOsIsWindows() {
  const hostOs = process.env.HOST_OS;
  return !!hostOs && hostOs.toLowerCase() === 'windows';
}

gulp.task('compile', gulp.series(() => {
  return gulp.src(['src/**/*.ts'])
    .pipe(tsProject())
    .js
    .pipe(gulp.dest('.tmp'));
}));

gulp.task('nodemon', gulp.series('compile', (cb) => {
  let called = false;

  return nodemon({
    script: './bin/www',
    watch: 'src/',
    ext: 'js html css ts pug',
    delay: '2500',
    legacyWatch: hostOsIsWindows(),
    tasks: (changedFiles) => {
      let tasks = [];
      changedFiles.forEach(file => {
        if (path.extname(file) === '.ts' && !~tasks.indexOf('compile'))
          tasks.push('compile');
      });
      return tasks;
    }
  }).on('start', () => {
    if (!called) {
      called = true;
      cb();
    }
  });
}));

gulp.task('clean', gulp.series(() => {
  return del(['.tmp']);
}));

gulp.task('serve', gulp.series('clean', 'nodemon'));

gulp.task('default', gulp.series('clean', 'compile'));
