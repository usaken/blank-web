import * as createError from 'http-errors';
import * as express from 'express';
import * as path from 'path';
import * as cookieParser from 'cookie-parser';
import * as logger from 'morgan';

import indexRouter from './routes/index';
import apiUsersRouter from './routes/api/users';

const app = express();

// view engine setup
app.set('views', path.join(__dirname, '..', 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, '..', 'public')));

app.use('/', indexRouter);
app.use('/api/users', apiUsersRouter);

// catch 404 and forward to error handler
app.use((_req, _res, _next) => {
  _next(createError(404));
});

// error handler
app.use((_err, _req, _res, _next) => {
  // set locals, only providing error in development
  _res.locals.message = _err.message;
  _res.locals.error = _req.app.get('env') === 'development' ? _err : {};

  // render the error page
  _res.status(_err.status || 500);
  _res.render('error');
});

export = app;
