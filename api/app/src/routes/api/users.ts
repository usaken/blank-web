import * as express from 'express';
const router = express.Router();

/* GET users listing. */
router.get('/', (_req, _res, _next) => {
  _res.send([
    { name: "一郎", age: 22 },
    { name: "二郎", age: 19 }
  ]);
});

export default router;
